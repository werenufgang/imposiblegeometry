// Copyright Epic Games, Inc. All Rights Reserved.

#include "ImposibleProjectGameMode.h"
#include "ImposibleProjectHUD.h"
#include "ImposibleProjectCharacter.h"
#include "UObject/ConstructorHelpers.h"

AImposibleProjectGameMode::AImposibleProjectGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AImposibleProjectHUD::StaticClass();
}
