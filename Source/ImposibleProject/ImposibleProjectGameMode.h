// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ImposibleProjectGameMode.generated.h"

UCLASS(minimalapi)
class AImposibleProjectGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AImposibleProjectGameMode();
};



