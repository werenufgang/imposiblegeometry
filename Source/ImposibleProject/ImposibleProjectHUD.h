// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "ImposibleProjectHUD.generated.h"

UCLASS()
class AImposibleProjectHUD : public AHUD
{
	GENERATED_BODY()

public:
	AImposibleProjectHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

